<?php

function fmgautosub_entity_view_alter(&$build, $type){
  if ($type == 'node' && $build['#bundle'] == 'branch') {
    if ($build['#view_mode'] == 'footer') {
      if (isset($build['#node']->field_branch_reg_office_option) && !empty($build['#node']->field_branch_reg_office_option)) {
        if ($build['#node']->field_branch_reg_office_option[LANGUAGE_NONE][0]['value'] == FALSE) {
          if (isset($build['field_address'])) {
            unset($build['field_address']);
          }
        }
      }
    }
    elseif (isset($build['field_branch_reg_office_address'])) {
      unset($build['field_branch_reg_office_address']);
    }
  }

//Show the page title outside the DS template and hide DS $title
//if ($type == 'node'){
//  if(($build['#bundle'] == 'page' || $build['#bundle'] == 'used_vehicle' || $build['#bundle'] == 'new_vehicle') && ($build['#view_mode'] == 'full' || $build['#view_mode'] == 'contact_page')) {
//    $bundle = $build['#bundle'];
//    $view_mode = $build['#view_mode'];
//    $layout = ds_get_layout($type, $bundle, $view_mode);
//    if (variable_get('ds_extras_hide_page_title', FALSE)) {
//      $page_title = &drupal_static('ds_page_title');
//      if (isset($layout['settings']['hide_page_title']) && $layout['settings']['hide_page_title'] == 1 && isset($build['title'][0]['#markup']) && ds_extras_is_entity_page_view($build, $type)) {
//        //Set page title
//        $page_title['title'] = strip_tags($build['title'][0]['#markup']);
//        //Hide DS title so you don't have both
//        unset($build['title']);
//      }
//    }
//  }
//}
}